<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "driver".
 *
 * @property int $id
 * @property string|null $fullname
 * @property string|null $birthday
 *
 * @property DriverBus[] $driverBuses
 * @property Bus[] $buses
 */
class Driver extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthday'], 'safe'],
            [['fullname'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'birthday' => 'Birthday',
        ];
    }

    /**
     * Gets query for [[DriverBuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDriverBuses()
    {
        return $this->hasMany(DriverBus::className(), ['driver_id' => 'id']);
    }

    /**
     * Gets query for [[Buses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBuses()
    {
        return $this->hasMany(Bus::className(), ['id' => 'bus_id'])->viaTable('driver_bus', ['driver_id' => 'id']);
    }
}
