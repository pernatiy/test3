<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bus".
 *
 * @property int $id
 * @property string|null $mark
 * @property string|null $model
 * @property string|null $year
 * @property int|null $speed
 *
 * @property DriverBus[] $driverBuses
 * @property Driver[] $drivers
 */
class Bus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year'], 'safe'],
            [['speed'], 'integer'],
            [['mark', 'model'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mark' => 'Mark',
            'model' => 'Model',
            'year' => 'Year',
            'speed' => 'Speed',
        ];
    }

    /**
     * Gets query for [[DriverBuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDriverBuses()
    {
        return $this->hasMany(DriverBus::className(), ['bus_id' => 'id']);
    }

    /**
     * Gets query for [[Drivers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Driver::className(), ['id' => 'driver_id'])->viaTable('driver_bus', ['bus_id' => 'id']);
    }
}
