<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m200416_213846_driver
 */
class m200416_213846_driver extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('driver', [
            'id' => $this->bigPrimaryKey(),
            'fullname' => $this->string(255),
            'birthday' => $this->date(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('driver');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200416_213846_driver cannot be reverted.\n";

        return false;
    }
    */
}
