<?php

use yii\db\Migration;

/**
 * Class m200416_213900_driver_bus
 */
class m200416_213950_driver_bus extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('driver_bus', [
            'driver_id' => $this->bigInteger(),
            'bus_id' => $this->integer(),
        ]);
        $this->addPrimaryKey('driver_bus', 'driver_bus', ['driver_id', 'bus_id']);
        $this->createIndex('bus_id', 'driver_bus', 'bus_id');
        $this->addForeignKey('fk_driver', 'driver_bus', 'driver_id', 'driver', 'id');
        $this->addForeignKey('fk_bus', 'driver_bus', 'bus_id', 'bus', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('driver_bus');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200416_213900_driver_bus cannot be reverted.\n";

        return false;
    }
    */
}
