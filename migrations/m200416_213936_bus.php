<?php

use yii\db\Migration;

/**
 * Class m200416_213936_bus
 */
class m200416_213936_bus extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bus', [
            'id' => $this->primaryKey(),
            'mark' => $this->string(255),
            'model' => $this->string(255),
            'year' => $this->integer(),
            'speed' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bus');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200416_213936_bus cannot be reverted.\n";

        return false;
    }
    */
}
