<?php

declare(strict_types=1);

namespace app\components;



use yii\web\HttpException;

class YaApi
{
    //const API_KEY = "67fc220a-9890-415b-ab6a-13e90f21d548";

    const API_KEY = "0847ad47-4a4c-490c-8ad4-0e6502dd5930";

    /***
     * Определяем координаты городов и кешируем их или кладем в БД
     * Воозвращаем координаты.
     * Примерный запрос: https://geocode-maps.yandex.ru/1.x/?apikey=self::API_KEY&geocode=$cityName&kind=city&results=1
     * Дока: https://tech.yandex.ru/maps/geocoder/doc/desc/concepts/about-docpage/
     */
    public static function getCityCoordByName(string $cityName): array
    {
        $cities = [
            'Москва' => [55.753215, 37.622504],
            'Казань' => [55.796289, 49.108795],
            'Питер' => [59.938951, 30.315635],
        ];
        if(!in_array($cityName, array_keys($cities))){
            throw new HttpException(900, 'YaApi не работает');
        }

        return $cities[$cityName];
    }

    /**
     * Метод для получения расстояния между городами по координатам
     * Вот так должен выглядеть запрос, но тк Яндекс не активирует ключ - нет возможности поиграится
     * ответ {"errors":["Key is not active"]} Пробовал несколько ключей
     * Запрос: https://api.routing.yandex.net/v1.0.0/distancematrix?origins=$latSrc,$lonSrc&destinations=$latDst,$lonDst&mode=transit&apikey=self::API_KEY
     * Дока: https://yandex.ru/routing/doc/distance_matrix/
     *
     * @param float $latSrc
     * @param float $lonSrc
     * @param float $latDst
     * @param float $lonDst
     * @return int|null
     */
    public static function getCityDistance(float $latSrc, float $lonSrc, float $latDst, float $lonDst): ?int
    {

        return 930;
    }

}