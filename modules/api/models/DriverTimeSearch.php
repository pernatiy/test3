<?php

namespace app\modules\api\models;

use app\models\Bus;
use app\models\DriverBus;
use yii\data\ActiveDataProvider;

/**
 * Модель для листинга водителей и времени прохождения пути по заданному расстоянию
 */
class DriverTimeSearch extends DriverApi
{
    /**
     * Creates data provider instance with search query applied
     *  ьлиприть
     *
     *
     *
     * kkkkkkk,
     * @return ActiveDataProvider
     */
    public function search($distance, $driver_id = null)
    {
        $query = DriverApi::find()->alias('d')
            ->select([
                'd.*',
                "(YEAR(CURRENT_DATE) - YEAR(d.birthday)) - (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(d.birthday, '%m%d')) `age`",
                '(:distance / MAX(b.speed)) `travel_time`',
            ])
            ->leftJoin(DriverBus::tableName() . ' db', 'db.driver_id = d.id')
            ->leftJoin(Bus::tableName() . ' b', 'b.id = db.bus_id')
            ->groupBy('d.id')
            ->orderBy('(:distance / MAX(b.speed))')
            ->asArray()
            ->params([':distance' => $distance])
        ;
        if(!is_null($driver_id)){
            $query->where(['d.id' => $driver_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);

        return $dataProvider;
    }
}
