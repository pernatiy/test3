<?php

namespace app\modules\api\models;

use yii\data\ActiveDataProvider;

/**
 * DriverApiSearch модель для листинга водителей
 */
class DriverApiSearch extends DriverApi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['fullname', 'birthday'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DriverApi::find()->alias('d')
            ->select([
                "d.*",
                "(YEAR(CURRENT_DATE) - YEAR(birthday)) - (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(birthday, '%m%d')) `age`"
            ])
            ->with('buses')
            ->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['fullname' => SORT_ASC]],
            'pagination' => [
                'pageSize' => 500,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'd.id' => $this->id,
            'd.birthday' => $this->birthday,
        ]);

        $query->andFilterWhere(['like', 'd.fullname', $this->fullname]);

        return $dataProvider;
    }
}
