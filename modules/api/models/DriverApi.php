<?php

declare(strict_types = 1);

namespace app\modules\api\models;

use app\models\Driver;
use yii\helpers\ArrayHelper;

class DriverApi extends Driver
{
    /**
     * @param float $travel_time - кол-во часов
     * @return string - читаемая строка 1д 6ч 8мин
     */
    public static function formatTravelTime(float $travel_time): string
    {
        if($travel_time <= 8 ){
            return floor($travel_time) .'ч '. (round(60 * fmod($travel_time, 1))) .'мин';
        }
        $days = floor($travel_time / 8);

        return $days . 'д ' . self::formatTravelTime($travel_time-8*$days);
    }


}