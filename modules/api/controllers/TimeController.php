<?php


namespace app\modules\api\controllers;


use app\components\YaApi;
use app\modules\api\models\DriverApi;
use app\modules\api\models\DriverTimeSearch;
use yii\rest\Controller;

class TimeController extends Controller
{

    public function actionIndex($city1, $city2, $driver_id = null)
    {
        $coord1 = YaApi::getCityCoordByName($city1);
        list($latSrc, $lonSrc) = $coord1;
        $coord2 = YaApi::getCityCoordByName($city2);
        list($latDst, $lonDst) = $coord2;
        $distance = YaApi::getCityDistance($latSrc, $lonSrc, $latDst, $lonDst);

        $searchModel = new DriverTimeSearch();
        $dataProvider = $searchModel->search($distance, $driver_id);
        $models = $dataProvider->getModels();
        foreach ($models as &$model) {
            $model['travel_time'] = DriverApi::formatTravelTime($model['travel_time']);
        }

        return $models;
    }



}