<?php

namespace app\modules\api\controllers;

use app\models\Driver;
use app\modules\api\models\DriverApi;
use app\modules\api\models\DriverApiSearch;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;

class DriverController extends ActiveController
{
    public $modelClass = DriverApi::class;

    /**
     * Переопределяем стандартные экшены
     *
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        ArrayHelper::remove($actions, 'index');

        return $actions;
    }

    /***
     * Список всех водителей можно передать сортировку или параметры в search модель
     * @return array
     */
    public function actionIndex()
    {
        $searchModel = new DriverApiSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $dataProvider->getModels();
    }

}