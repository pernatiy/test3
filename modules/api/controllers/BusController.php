<?php


namespace app\modules\api\controllers;


use app\modules\api\models\BusApi;
use app\modules\api\models\DriverApiSearch;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;

class BusController extends ActiveController
{
    public $modelClass = BusApi::class;

}